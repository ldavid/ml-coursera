# ML-Coursera

Machine Learning algorithms implemented for the Machine Learning class at coursera.
These algorithms were implemented using the [Octave](https://www.gnu.org/software/octave/) programming language, following the suggestion of the professor.