function [theta, J_history] = gradientDescentMulti(X, y, theta, alpha, num_iters)
%GRADIENTDESCENTMULTI Performs gradient descent to learn theta
%   theta = GRADIENTDESCENTMULTI(x, y, theta, alpha, num_iters) updates theta by
%   taking num_iters gradient steps with learning rate alpha

    m          = length(y);           % number of training examples
    parameters = length(theta);       % number of parameters
    J_history = zeros(num_iters, 1);

    for iter = 1:num_iters

        % Find the error frame between currrent configuration and goal.
        e = X * theta -y;

        % Replicate 'e', we want to make the dot product of it with each column of X.
        e = e .* ones(m, parameters);

        % Transpose it, so it matches 'theta'.
        delta = dot(e, X)';

        % Minimizing, obviously...
        theta = theta - (alpha / m) * delta;

        % Save the cost J in every iteration
        J_history(iter) = computeCostMulti(X, y, theta);

    end
end
