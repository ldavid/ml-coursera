function [J, grad] = costFunctionReg(theta, X, y, lambda)
%// COSTFUNCTIONREG Compute cost and gradient for logistic regression with regularization
%// J = COSTFUNCTIONREG(theta, X, y, lambda) computes the cost of using
%// theta as the parameter for regularized logistic regression and the
%// gradient of the cost w.r.t. to the parameters. 

	s = sigmoid(X * theta);
	m = length(y);

	J 	 = -1 / m * sum(y .* log(s) + (1 -y) .* log(1 -s)) + lambda/2/m * sum(theta(2:end) .^ 2);
	grad =  1 / m * sum((s -y) .* X) + lambda / m * [0 theta(2:end)'];

end
